import { ColliV1Page } from './app.po';

describe('colli-v1 App', function() {
  let page: ColliV1Page;

  beforeEach(() => {
    page = new ColliV1Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
